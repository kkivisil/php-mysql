<?php

function autendi(){
	global $myurl;
	global $connection;
	global $db;
	mysqli_select_db($connection, $db);
	$username=$_POST['username'];
	$passwd=$_POST['passwd'];
	if($username && $passwd){
	$passwd1=hash('sha1', $passwd);
	$username=stripslashes($username);
	$passwd1=stripslashes($passwd1);
	$username=mysqli_real_escape_string($connection, $username);
	$passwd1=mysqli_real_escape_string($connection, $passwd1);
	$check="SELECT * FROM kkivisil_kasutajad WHERE kasutaja ='$username' and parool ='$passwd1'";
	$stillcheck=mysqli_query($connection,$check);
	$count=mysqli_num_rows($stillcheck);
	if($count==1){
		$_SESSION['username']=$username;
		$_SESSION['id']=0;
		header("Location: $myurl");
		$_SESSION['notices'][]="Tere ".htmlspecialchars($username).", sisselogimine õnnestus!";
		include('view/pealeht.html');
		echo "Sisselogimine õnnestus";
	}else{
		    	
    	$errors[]="Vale info, proovi uuesti.";	
    	include("view/sisselogimine.html");
	}  	
}else{

	$errors[]="Täida mõlemad väljad";
	include("view/sisselogimine.html");
}
  		
}
function show_login(){
	global $myurl;
include("view/sisselogimine.html");
}

function logout(){
lopeta_sessioon();
global $myurl;
header("Location: $myurl");
}

function show_reg_vorm(){	
	global $myurl;
	include("view/registreerimine.html");
}

function show_minust(){	
	global $myurl;
	include("view/minust.html");
}

function show_graafik(){	
	global $myurl;
	include("view/graafik.html");
}

function show_bussid(){	
	global $myurl;
	include("view/bussid.html");
}

function show_kontakt(){	
	global $myurl;
	include("view/kontakt.html");
}

function show_pealeht(){	
	global $myurl;
	include("view/pealeht.html");
}

function show_info(){	
	global $myurl;
	global $connection;
 	$soitjad=array();
 	$query ="SELECT * FROM kkivisil_info";
 	$num = 0;
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
  		$num = mysqli_num_rows($result);
  		$soitjad[]=$row;
  	}
 	$sisu = "";
 	$sisu .=	"<tr><th>|Buss|</th><th>|Mootor|</th><th>   |KW|</th></tr>";
  	foreach($soitjad as $soitja){
		$sisu .= "<tr><td>";
		$pildid = get_pilt();
		$sisu .=  "<a href=";
		foreach($pildid as $pilt){
			if($soitja['id']==$pilt['Buss_ID']){
				$sisu .= $pilt['Pilt'];	
				break;
			}	
		}
		$sisu .= ">". $soitja['Buss'] ."</a>". "</td>";
		$sisu .= "<td>".$soitja['Mootor']."</td><td>".$soitja['KW']."</td></tr>";
	}	
	
	include("view/info.html");
}

function get_pilt(){
	global $connection;
 	$pildid=array();
 	$query ="SELECT * FROM kkivisil_bussipilt";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
  		$num = mysqli_num_rows($result);
  		$pildid[]=$row;
  	}
  	return $pildid;
}
   

function alusta_sessioon(){
	
	session_start();
	}
	
	
	function lopeta_sessioon(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
}

function connect_db(){
  global $connection;
  $host="localhost";
  $user="test";
  $pass="t3st3r123";
  $db="test";
  $connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- ".mysqli_error());
  mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}



function registreeri(){
	
	global $connection;
	global $myurl;
	
	$regUser = $_POST['kasutajanimi'];
	$regPass = $_POST['parool'];
	$regPass_2 = $_POST['parool2'];
	$regName = $_POST['eesnimi'];
	$regSname = $_POST['perenimi'];
	$regAge = $_POST['vanus'];
	$verify = mysqli_query($connection, "SELECT kasutaja FROM kkivisil_kasutajad");
	
	if($regUser && $regPass && $regPass_2 && $regName && $regSname && $regAge ) {


	
		if ($regPass==$regPass_2) {
			
			$regPassEnc = hash('sha1', $regPass);
			$level = "user";
		
			$sql = "INSERT INTO kkivisil_kasutajad(kasutaja,parool,eesnimi,perenimi,vanus,level) values ('$regUser' , '$regPassEnc' , '$regName' , '$regSname' , '$regAge' , '$level')";
			$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));
			echo "Kasutaja registreerumine õnnestus.";

		}else
			echo "Salasõnad on erinevad";
	}  
	else echo "Mitte sobilikud andmed";

	include('view/registreerimine.html');
}

function kontrolli_saadavus($regUser){
global $connection;
$username=mysqli_real_escape_string($connection, $regUser);

$query ="SELECT * level FROM kkivisil_kasutajad WHERE regUser='$regUser'";
$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
  if (mysqli_num_rows($result)>0) {
    return true;
  }else { 
    return false;
  }
}


?>